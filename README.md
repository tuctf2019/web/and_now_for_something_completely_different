# And Now, For Something Completely Different

Hello and welcome, it's time for RCE via Tornado template injection, brought to you by Tyler's website

* Desc: We all know Black Friday is the time for shopping. Can you find us a flag on this online store?
* Flag: `TUCTF{4lw4y5_60_5h0pp1n6_f0r_fl465}`

## How To:
1. Holy crap there's so much here so many paths. Ignore everything but the source code comment to `/welcome/test`
2. Going to this URL gives you a page that says `Welcome test`. At this point you should realize we're taking your input. Try a few things, look around, and realize it's a template. Easiest way to do that: `/welcome/{{ 7 + 7 }}` This returns `Welcome 14`
3. Haha it's time to hack! Now you have to make the jump to realize we want you to run commands on the box this is hosted on. `/welcome/{ % import os % }{{ os.popen("ls -l").read() }}` shows you a file called `flag.txt`...
4. You cat the file (`/welcome/{%25 import os %25}{{ os.popen("cat flag.txt").read() }}`) and congrats, it's the flag. Note: the `%25` is to give you the URL encoded `%` I omitted that in step 3 for clarity. You do not need to URL encode anything not specified in my solution, there is no filtering.

This challenge has a lot of leaps of logic, but the hardest one is realizing it's a template. I'm not really sure how to hint at this at this point except note that the input they give us is unsanitized. One thing that's another basic check besides `{{ 7 + 7 }}` is `{{ self }}` which actually tells you it's Tornado and Python 2.7 while technically 404ing
